package source;

/**
 * 
 * @author bzaganjori
 */

public class VertexDoesNotExistException extends Exception {

    private static final long serialVersionUID = -5780582101860603802L;

    public VertexDoesNotExistException() {
        super("Vertex does not exist.");
    }
}
