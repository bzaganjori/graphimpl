package source;

/**
 * 
 * @author bzaganjori
 */

public class VertexExistsException extends Exception {

    private static final long serialVersionUID = 4153393919450953796L;

    public VertexExistsException() {
        super("Vertex already exists.");
    }

}
