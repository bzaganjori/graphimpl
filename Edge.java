package source;

/**
 * 
 * @author bzaganjori
 */

public class Edge {

    private Vertex origin;
    private Vertex destination;
    private int weight;
    private Edge next;
    
    /**
     * Create new Edge object.
     */
    public Edge() {
        this.origin = null;
        this.destination = null;
        this.weight = 0;
        this.next = null;
    }

    /**
     * Create new Edge object.
     * @param u origin vertex
     * @param v destination vertex
     * @param e weight
     * @param n next edge in linked list
     */
    public Edge(Vertex u, Vertex v, int e, Edge n) {
        this.origin = u;
        this.destination = v;
        this.weight = e;
        this.next = n;
    }
    
    /**
     * 
     * @return 
     */
    public Vertex getOrigin() {
        return this.origin;
    }

    public Vertex getDestination() {
        return this.destination;
    }

    public int getWeight() {
        return this.weight;
    }

    public Edge getNext() {
        return this.next;
    }

    public void setEndPoints(Vertex u, Vertex v) {
        this.origin = u;
        this.destination = v;
    }

    public void setWeight(int e) {
        this.weight = e;
    }

    public void setNext(Edge e) {
        this.next = e;
    }

    public boolean equals(Edge e) {
        return e.getOrigin().getItem().equals(origin.getItem())
                && e.getDestination().getItem().equals(destination.getItem())
                && e.getWeight() == this.weight;
    }

}
