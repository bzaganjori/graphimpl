package source;

/**
 * 
 * @author bzaganjori
 */

public class EdgeDoesNotExistException extends Exception {

    private static final long serialVersionUID = -8084190120529614529L;

    public EdgeDoesNotExistException() {
        super("Edge does not exists.");
    }

}
