package source;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 
 * @author bzaganjori
 */

public class Graph {

    private final HashMap<Vertex, Edge> map;

    public Graph() {
        this.map = new HashMap<Vertex, Edge>();
    }
    
    public void insertVertex(Vertex v) throws VertexExistsException {
        if (vertexExists(v)) 
            throw new VertexExistsException();
        map.put(v, null);
    }
    
    public void insertVertex(String s) throws VertexExistsException {
        Vertex v = new Vertex(s);
        insertVertex(v);
    }
    
    public void insertEdge(Edge e) throws VertexDoesNotExistException {
        if (!vertexExists(e.getOrigin()) || !vertexExists(e.getDestination())) 
            throw new VertexDoesNotExistException();
        if (edgeExists(e)) 
            return;
        e.setNext(map.get(e.getOrigin()));
        map.put(e.getOrigin(), e);
    }

    public void insertEdge(Vertex u, Vertex v, int o) throws VertexDoesNotExistException {
        Edge e = new Edge(u, v, o, null);
        insertEdge(e);
    }

    public void removeVertex(Vertex u) throws VertexDoesNotExistException {
        if (!vertexExists(u)) 
            throw new VertexDoesNotExistException();
        map.remove(findVertex(u.getItem()));
    }

    public void removeEdge(Edge e) throws EdgeDoesNotExistException {
        if (!edgeExists(e)) 
            throw new EdgeDoesNotExistException();
        
        Edge head = map.get(e.getOrigin());
        Edge p = head;
        Edge q = head.getNext(); 
        
        while (q != null) {
            if (head.equals(e)) {
                map.put(e.getOrigin(), head.getNext());
                return;
            } else if (q.equals(e)) {
                p.setNext(q.getNext());
                return;
            } else {
                p = q;
                q = q.getNext();
            }
        }
    }

    public List<Vertex> endVertices(Vertex u)  throws VertexDoesNotExistException {
        if (!map.containsKey(u)) 
            throw new VertexDoesNotExistException();
        List<Vertex> list = new ArrayList<Vertex>();
        Edge newEdge = map.get(u);
        while (newEdge != null) {
            list.add(newEdge.getDestination());
            newEdge = newEdge.getNext();
        }
        return list;
    }

    public Vertex opposite(Vertex u, Edge e) throws VertexDoesNotExistException, EdgeDoesNotExistException {
        if (!map.containsKey(u)) 
            throw new VertexDoesNotExistException();
        if (!edgeExists(e)) 
            throw new EdgeDoesNotExistException();
        
        if (u == e.getOrigin()) 
            return e.getDestination();
        else if (u == e.getDestination()) 
            return e.getOrigin();
        else 
            return null;
    }

    public boolean areAdjacent(Vertex u, Vertex v) throws VertexDoesNotExistException {
        return endVertices(u).contains(v);
    }

    public void replace(Vertex v, String o) throws VertexDoesNotExistException {
        if (!map.containsKey(v)) 
            throw new VertexDoesNotExistException();
        v.setItem(o);
    }

    public List<Edge> incidentEdges(Vertex v) throws VertexDoesNotExistException {
        if (!map.containsKey(v)) 
            throw new VertexDoesNotExistException();
        List<Edge> list = new ArrayList<Edge>();
        Edge newEdge = map.get(v);
        while (newEdge != null) {
            list.add(newEdge);
            newEdge = newEdge.getNext();
        }
        return list;
    }

    public List<Vertex> vertices() {
        List<Vertex> list = new ArrayList<Vertex>();
        Iterator<Vertex> itr = keySetIterator();
        while (itr.hasNext()) 
            list.add(itr.next());
        return list;
    }

    public List<Edge> edges() {
        List<Edge> list = new ArrayList<>();
        Set<Entry<Vertex, Edge>> set = map.entrySet();
        Iterator<Entry<Vertex, Edge>> itr = set.iterator();
        while (itr.hasNext()) {
            Entry<Vertex, Edge> entry = itr.next();
            Edge edge = entry.getValue();
            while (edge != null) {
                list.add(edge);
                edge = edge.getNext();
            }
        }
        return list;
    }

    public String allConnections() {
        String str = "";
        Iterator<Vertex> itr = keySetIterator();
        while (itr.hasNext()) {
            Vertex vert = itr.next();
            Edge edge = map.get(vert);
            while (edge != null) {
                str += edge.getOrigin().getItem()+ " "
                        + edge.getDestination().getItem() + " "
                        + edge.getWeight() + "\n";
                edge = edge.getNext();
            }
        }
        return str;
    }

    public String connectionsFrom(String v) throws VertexDoesNotExistException {
        String str = "";
        Iterator<Vertex> itr = keySetIterator();
        while (itr.hasNext()) {
            Vertex vert = itr.next();
            if (vert.getItem().equals(v)) {
                Edge edge = map.get(vert);
                while (edge != null) {
                    str += edge.getOrigin().getItem()+ " "
                            + edge.getDestination().getItem()+ " "
                            + edge.getWeight() + "\n";
                    edge = edge.getNext();
                }
            }
        }
        return str;
    }
    
    private Iterator keySetIterator() {
        return map.keySet().iterator();
    }

    public String connectionsBetween(String s1, String s2) throws VertexDoesNotExistException {
        String str = "";
        Iterator<Vertex> itr = keySetIterator();
        while (itr.hasNext()) {
            Vertex vert = itr.next();
            if (vert.getItem().equals(s1)) {
                Edge edge = map.get(vert);
                while (edge != null) {
                    if (edge.getDestination().getItem().equals(s2)) {
                        str += edge.getOrigin().getItem() + " "
                                + edge.getDestination().getItem() + " "
                                + edge.getWeight() + "\n";
                    }
                    edge = edge.getNext();
                }
            }
        }
        return str;
    }

    public boolean removeCity(String s) throws VertexDoesNotExistException {
        Iterator<Vertex> itr = keySetIterator();
        while (itr.hasNext()) {
            Vertex vert = itr.next();
            if (vert.getItem().equals(s)) {
                removeVertex(vert);
                return true;
            }
        }
        return false;
    }

    public boolean removeConnection(String v1, String v2, String weight) throws EdgeDoesNotExistException {
        Iterator<Vertex> itr = keySetIterator();
        while (itr.hasNext()) {
            Vertex vertex = itr.next();
            Edge edge = map.get(vertex);
            while (edge != null) {
                if (edge.getOrigin().getItem().equals(v1)
                        && edge.getDestination().getItem().equals(v2)
                        && edge.getWeight() == Integer.parseInt(weight)) {
                    removeEdge(edge);
                    return true;
                }
                edge = edge.getNext();
            }
        }
        return false;
    }

    @Override
    public String toString() {
        String str = "";
        Iterator<Vertex> itr = keySetIterator();
        while (itr.hasNext()) {
            Vertex vert = itr.next();
            Edge edge = map.get(vert);
            String vertex = vert.getItem() + " : ";
            if (edge == null) {
                str += vertex + "\n";
            }
            while (edge != null) {
                str += vertex;
                str += edge.getDestination().getItem() + " ";
                str += edge.getWeight() + " ";
                str += "\n";
                edge = edge.getNext();
            }
        }
        str += "\n";
        return str;
    }

    private Vertex findVertex(String v) {
        Iterator<Vertex> itr = keySetIterator();
        while (itr.hasNext()) {
            Vertex vertex = itr.next();
            if (vertex.getItem().equals(v)) 
                return vertex;
        }
        return null;
    }

    private Edge findEdge(Vertex endpointA, Vertex endpointB, int weight) {
        List<Edge> list = edges();
        for (Edge s : list) {
            if (s.getOrigin().equals(endpointA)
                    && s.getDestination().equals(endpointB)
                    && s.getWeight() == weight) {
                return s;
            }
        }
        return null;
    }

    private boolean vertexExists(Vertex v) {
        return map.containsKey(v);
    }
    
    private boolean edgeExists(Edge e) {
        Edge temp = map.get(e.getOrigin());
        while (temp != null) {
            if (e.equals(temp)) 
                return true;
            temp = temp.getNext();
        }
        return false;
    }
    
//    public void createConnection(String v1, String v2, int weight)
//            throws VertexExistsException {
//        if (!vertexExists(v1)) insertVertex(new Vertex((V) v1));
//        if (!vertexExists((V) v2)) insertVertex(new Vertex((V) v2));
//        Vertex vert1 = findVertex((V) v1);
//        Vertex vert2 = findVertex((V) v2);
//        try {
//            insertEdge(vert1, vert2, weight);
//        } catch (VertexDoesNotExistException e) { }
//
//    }

}
