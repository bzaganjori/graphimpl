package source;

/**
 * 2016/08/24 
 * @author bzaganjori
 */

public class Vertex {
    
    private String item;

    /**
     * Creates a Vertex object.
     * @param item specified string
     */
    public Vertex(String item) {
        this.item = item;
    }

    /**
     * Return the item value.
     * @return value of object
     */
    public String getItem() {
        return this.item;
    }

    /**
     * Set the item value
     * @param item new string
     */
    public void setItem(String item) {
        this.item = item;
    }

}
