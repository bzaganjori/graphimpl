package source;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    static Graph graph = new Graph();

    public static void main(String[] args) throws Exception {
//        test();
        program();
    }

    private static void program() throws
            IOException,
            VertexExistsException,
            VertexDoesNotExistException,
            EdgeDoesNotExistException {

        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
        String input;
        String[] parts;

        pl("#########################");
        pl("# City Graph 	         ");
        pl("# 'USAGE' for help.      ");
        pl("#########################");
        while (true) {
            p(": ");

            input = br.readLine();
            parts = input.split(" ");

            try {
                if (parts[0].equals("+")) {
                    if (parts.length != 5) {
                        invalidInput();
                    } else {
                        try {
//                            graph.createConnection(parts[1], parts[2], Integer.parseInt(parts[3]), parts[4]);
                        } catch (NumberFormatException e) {
                            invalidInput();
                        }
                    }
                } else if (parts[0].equals("-")) {
                    switch (parts.length) {
                        case 2:
                            graph.removeCity(parts[1]);
                            break;
                        case 5:
                            graph.removeConnection(parts[1], parts[2], parts[4]);
                            break;
                        default:
                            invalidInput();
                            break;
                    }
                } else if (parts[0].equals("?")) {
                    switch (parts.length) {
                        case 1:
                            p(graph.allConnections());
                            break;
                        case 2:
                            p(graph.connectionsFrom(parts[1]));
                            break;
                        case 3:
                            // TODO: FIND SHORTEST PATH BETWEEN CITIES
//                            p(graph.connectionsBetween(parts[1], parts[2]));
//                            p(graph.shortestPath(parts[1], parts[2]));
                            break;
                        default:
                            invalidInput();
                            break;
                    }
                } else if (parts[0].equals("QUIT") || parts[0].equals("quit")) {
                    break;
                } else if (parts[0].equals("USAGE") || parts[0].equals("usage")) {
                    usage();
                } else {
                    invalidInput();
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                invalidInput();
            }
        }
        pl("############");
        pl("# Goodbye. #");
        pl("############");
    }

    private static void invalidInput() {
        pl("Invalid input. Try 'USAGE' for help.");
    }

    private static void usage() {
        pl("+ YYZ JFK 120 plane : Add connection from YYZ to JFK, 120 minutes "
                + "by plane");
        pl("- YYZ : Remove city YYZ from system.");
        pl("- YYZ JFK 120 plane : Remove specific connection between city.");
        pl("? YYZ : List all connections from YYZ");
        pl("? YYZ LAX : List fastest route between cities.");
        pl("? : List all connections in the system.");
        pl("QUIT : Quit.");
        pl("USAGE : Display help dialoge.");
    }

    /* private static void test() 
            throws ExceptionVertexExists, ExceptionVertexDoesNotExist, 
            ExceptionEdgeDoesNotExist {
        Vertex<String> v1 = new Vertex<String>("YYZ");
        Vertex<String> v2 = new Vertex<String>("LAX");
        Vertex<String> v3 = new Vertex<String>("CYZ");

        // Test insertion
        graph.insertVertex(v1);
        graph.insertVertex(v2);
        graph.insertVertex(v3);
        graph.insertEdge(v1, v2, 120, "plane");
        graph.insertEdge(v1, v3, 60, "plane");
        graph.insertEdge(v2, v3, 60, "plane");
        p(graph.toString());

        // Test edge removal
        Edge<String, Integer> edge = graph.findEdge(v1, v2, 120, "plane");
        Edge<String, Integer> edge2 = graph.findEdge(v2, v3, 60, "plane");
        graph.removeEdge(edge);
        graph.removeEdge(edge2);
        p(graph.toString());
        
        // Test vertex removal
        graph.removeVertex(v2);
        p(graph.toString());
        
        // Test end vertices method
        List<Vertex<String>> list = graph.endVertices(v1);
        for (Vertex<String> s : list) p(s.getItem().toString());
        
        // Test opposite method
        p(graph.opposite(v1, edge).getItem().toString());
        
        // Test areAdjecent method
        if (graph.areAdjacent(v1, v2) == true) p("true");
        
        // Test incidentEdges method
        List<Edge<String, Integer>> list = graph.incidentEdges(v1);
        for (Edge<String, Integer> s : list) {
        	p(s.getEndpointA().getItem().toString() + " "
        			+ s.getEndpointB().getItem().toString() + " "
        			+ s.getDistance().toString() + " "
        			+ s.getVehicle());
        }
        
        // Test vertices method
        List<Vertex<String>> list = graph.vertices();
        for (Vertex<String> v : list) p(v.getItem().toString());
        
        // Test edges method
        List<Edge<String, Integer>> list = graph.edges();
        for (Edge<String, Integer> s : list) {
        	p(s.getEndpointA().getItem().toString() + " "
        			+ s.getEndpointB().getItem().toString() + " "
        			+ s.getDistance().toString() + " "
        			+ s.getVehicle());
        }
        
        // Test connectionsBetween method
        p(graph.connectionsBetween("YYZ", "LAX"));
        p(graph.connectionsFrom("LAX"));
        p(graph.allConnections());
        graph.removeConnection("YYZ", "LAX", "120", "plane");
        p(graph.toString());
        
        
    } */
    private static void pl(String s) {
        System.out.println(s);
    }

    private static void p(String s) {
        System.out.print(s);
    }

}
